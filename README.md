# **WORK IN PROGRESS!**
This is my first major programming project. Use at your own risk.
Any advice, issue, pull request, etc. is very welcome

# Getting Started
## what is network-caretaker.py?
This python tool is aimed to make network automation in multi-vendor environments less of a hassle by using FOSS.  
With only one command syntax (based on aristaEOS/ciscoIOS) and knowing the templates you can generate  
and or execute multiple commands in privEXEC and config-mode or replace full configs and backup the output for multiple vendors.  
It uses either nornir or ansible as automation frameworks, capabilties are very similar so they can be used redundantly.  
You can use network-caretaker.py in bash or powershell scripts and automate the execution by using ssh and gpg keys. (in Progress)  
Includes some example sites and netlab topologies.  
Compatible with Fedora, Debian, LinuxMint, Ubuntu (and not yet Windows).  

## installation
Download the install script for your OS. (Or clone this repo.)
Go to the directory where the install script is.
Run the install script. (It will clone this repo if that has not happened yet.)

currently supported:
- linux (install/install.sh)

### site_dir structure (example site)

## usage
### enter environment
Edit $NETWORKCARETAKER_SITE in network_venv/bin/postacivate to the directory where inventory_dir, secrets and custom config are located.
Enter your python virtual environment.
```
# linux
. ~/.venv_network/bin/activate

# Windwows
. ~\.venv_network\script\Activate.ps1
```
Now you can run *network-caretaker.py* from anywhere.

In linux you can also run ansible playbooks with the -A or --ansible option.
This way it will still have autocompletion and some other features.
helpMESSAGE

### 

# features
## supported netOSes
| **netOS**          | **Tested Show Commands**     | **Tested Configuration**                    |
|--------------------|------------------------------|---------------------------------------------|
| **aristaEOS**      | Single: [X] Compilation: [X] | PrivEXEC: [X] Config: [X] Full Replace: [X] |
| **ciscoIOS**       | Single: [ ] Compilation: [ ] | PrivEXEC: [ ] Config: [ ] Full Replace: [ ] |
| **huaweiYUNSHANOS**| Single: [X] Compilation: [ ] | PrivEXEC: [ ] Config: [ ] Full Replace: [ ] |
| **huaweiVRP**      | Single: [X] Compilation: [X] | PrivEXEC: [X] Config: [ ] Full Replace: [ ] |
| **dellOS10**       | Single: [X] Compilation: [X] | PrivEXEC: [X] Config: [X] Full Replace: [ ] |
| **dellOS9**        | Single: [X] Compilation: [ ] | PrivEXEC: [X] Config: [ ] Full Replace: [ ] |
| **dellOS6**        | Single: [X] Compilation: [X] | PrivEXEC: [X] Config: [ ] Full Replace: [ ] |
| **arubaOS**        | Single: [ ] Compilation: [ ] | PrivEXEC: [ ] Config: [ ] Full Replace: [ ] |

## template based configuration and troubleshooting
* [X] includes multivendor jinja2 templates
* [X] has single show and config commands, compilations, utility templates
* [X] written so templates can be used in privEXEC,config-mode and config generation
* [X] supports site and generale template folder (contents will be merged temporarily)
* [X] easily adaptable for other vendors/topologies
* [example template](./templates/show_running-config.j2) 

## connection types
### ansible.netcommon.network_cli
* abiltiy to execute show commands:
    * [X] single commands
    * [x] compilations
* abiltiy to configure devices in config mode:
    * [x] single commands
    * [X] compilations
* [X] ability to replace full configs (based on vendor capability)
* [X] abiltiy to configure devices starting from privEXEC mode if necessary

### nornir_netmiko
* abiltiy to execute show commands:
    * [x] single commands
    * [ ] compilations
* abiltiy to configure devices in config mode:
    * [x] single commands
    * [ ] compilations
* [ ] ability to replace full configs (based on vendor capability)
* [ ] abiltiy to configure devices starting from privEXEC mode if necessary

## management of secrets
### gpg (nornir)
* [ ] will prompt for default SSH&enable password if not set
* [X] secrets are stored in gpg encrypted file (symmetrical or asymmetrical)
* [X] secrets loaded in gpg-agent do not to be interactively put in (usable in other scripts)
* [ ] passes secrets to ansible

### ansible_vault
* [X] secrets are stored in ansible_vault encrypted file (symmetrical)
* [X] prompts for vault password when used

## backup options
* [X] toggle backup of output via prompt or parameter
* [ ] backups of configs are made before config changes automatically

## interacivity
* [X] select a template through autocomplete and dropdown menu when none are specified
* interactive mode:
    * [X] get prompted for all options
    * [ ] get info and instructions on all options

## [nornir <-> ansible] compatibility
* [X] using ansible playbooks or nornir runbooks has the same results
* [X] network-caretaker.py parameters closely resemble ansible-playbook
* [X] network-caretaker.py options are passed to ansible-playbook in ansible mode
* [ ] modify playbooks with network-caretaker.py
* [X] nornir is compatible with jinja templates containing ansible variables
* [X] nornir uses the same inventory file as ansible through nornir_ansible AnsibleInventory plugin
* inventory filtering:
    * [X] nornir supports ansible syntax for hosts and groups
    * [X] nornir supports ansible syntax for ':','!','&'
    * [ ] nornir supports ansible syntax for wildcard and vars filtering
    * [X] includes a config and playbooks with similar functionality to network-caretaker.py
    * [X] execute playbooks with network-caretaker.py

## example sites/labs
* includes example sites with the nececassary file structure, files and vars
* includes netlab topology files for labbing example sites

## SSH??
* ssh:
    * [ ] script to generate config draft from inventory
    * [ ] includes templates to create ssh user, keys and passwords on network devices 
    * [ ] ??? jumphosting??
    * [X] python venv postactivate script sets up custom ssh config path
    * [ ] support for fully key based setup

# Forking (and integrating upstream changes)
If you plan to modify this it is recommended to do a fork of this repo so you can integrate upstream changes easilly.

## manual
Example for integrating upstream changes:
```
git remote add upstream https://gitlab.com/k.hajialireza/network-automation.git
git fetch upstream
git checkout main
git rebase upstream/main
```

## CI/CD pipeline
- Create a pipeline file for your fork to integrate upstream changes.

example: gitlab
```yml
---
#.gitlab-ci.yml
stages:
  - update

update_repo:
  stage: update
  script:
    #- git config --global user.email "you@example.com"
    #- git config --global user.name "Your Name"
    - git remote add upstream https://gitlab.com/k.hajialireza/network-automation.git
    - git fetch upstream
    - git switch trunk
    - git rebase upstream/main
  only:
    - schedules
...
```

- Then schedule your pipeline

## Roadmap (not in order)
* windows support
* multiple inventories (in python)
* full ansible vault features (lookup those)
* git automation
* netlab example topology with linux clients
* netlab topology arista l3ls  topology multivendor l2ls     inventory mc-lag, stack
* check und diff? -> dry-run?
* netbox inventories
* case for netmiko not working? -> paramiko
* AVD&PyAVD integration
* templates and logic from netlab
* prompt answers (save, reboot, ....)
* man page installation (und pip ugrade?) in die docs
* python pip package-repo?
* code review security (secrets loadin/unloading)
    * safety!!!  what is safe? how safe?
        * unload all password holding vars right after using them
* implement other connection types than netmiko (netconf, napalm, scrapli, scrapli-netconf, paramiko)
* fallback to ansible when nornir does not work
* fallback to other connection type when one does not work
* jenkins example vor CI/CD

# *BUGS*
* When no default become Password is set, you get prompted for it, atm it prompts for every device
* nornir_netmiko_send and config do not accept multiline

# *TODO*

## test
* multiple templates:
    * [ ] ansible raw playbook
    * [ ] network-careteker.py -A
    * [ ] python
    * [ ] input and file
* template_dir merging
    * [ ] without dirs
    * [X] with dirs
* inventory and secret parsing:
    * [ ] group_vars and host_vars
    * [ ] no encpryted file and no data in inventory
    * [ ] ?? all options here
* where and when are passwords loaded/unloaded?:
    * [ ] password vars
    * [ ] inventory vars

## improve
* config backup
    * [ ] add backup and backup_dir (same as show run backup) to config.yml and replace-config.yml
* save:
    * [ ] add auto save on config
* pass network-caretaker parameters to ansible:
    * [ ] pass Options to ansible
    * [ ] pass function selection by name to ansible (so no playbook has to be selected)
    * [ ] pass secrets???
* generate templates should include those in utilities
    * [ ] nornir
    * [ ] ansible
* dependencies:
    * [ ] finish fedora dependencies
    * [ ] finish debian dependencies
    * [ ] finish windows dependencies
* README:
    * [ ] ref: https://medium.com/chingu/keys-to-a-well-written-readme-55c53d34fe6d
    * [ ] missing connection possibiliteis, ssh, paramiko, netmiko, netconf, restapi, napalm, scrapli, ...) in netos table
    * [ ] directory structure for site
        * [ ] necessary files
    * [ ] network-caretaker.py
        * [ ] different usages
        * [ ] screenshots/code blocks  of outputs
    * [ ] ansible tutorial:
        * [ ] screenshots
        * [ ] network-caretaker.py
        * [ ] manual
    * [ ] templates:
        * [ ] names
        * [ ] structure
* debugging:
    * [ ] mypy usage
* [ ] argparse helps for program and functions
* [ ] doc for functions
* [ ] typehintig
* [ ] complete adding of ansible vars (what vars should be included aswell?)
* [ ] conditional exit statement for those vendors that need it and have to use privEXEC mode and dont have exit in their configs 
* [X] new line differentiation for os6 between cfg and privEXEC should be conditional on whether they are at the end or in the middle and rendered differently.

# SHOUTOUTS (where to put their licenses?)

nornir
netlab
arista.avd
ansible
gnupg
