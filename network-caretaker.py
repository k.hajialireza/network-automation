#!/usr/bin/env python3

import typing as t #typehinting
#import collections.abc as c #typehinting
import gnupg
from getpass import getpass
import os
#import subprocess
import tempfile
import shutil
import yaml
import json
import argparse
from prompt_toolkit import prompt
from prompt_toolkit.completion import NestedCompleter, WordCompleter
from nornir import InitNornir
from nornir.core import Nornir
from nornir.core.filter import F
from nornir.core.inventory import Defaults, ConnectionOptions#, Host, Group
from nornir.core.plugins.inventory import InventoryPluginRegister
from nornir.core.task import Task, AggregatedResult, MultiResult#, Result
from nornir_utils.plugins.functions import print_result
from nornir_utils.plugins.tasks.files import write_file
from nornir_jinja2.plugins.tasks import template_file
from nornir_ansible.plugins.inventory.ansible import AnsibleInventory
from nornir_netmiko.tasks import netmiko_send_command, netmiko_send_config

def debug_inventory(nr: Nornir, inventory_element_type: str = 'default', host_or_group: str = 'default') -> None:
    if inventory_element_type == 'group':
        inventory_element = nr.inventory.groups[host_or_group]
    if inventory_element_type == 'host':
        inventory_element = nr.inventory.hosts[host_or_group]
    else:
        inventory_element = nr.inventory.defaults
    print(f"\n\n^^^^^^^^ {inventory_element_type=} {host_or_group} ^^^^^^^^")
    print(f"-------- {host_or_group=} dict --------")
    print(f"{json.dumps(inventory_element.dict(), indent=2)}")
    print(f"-------- {host_or_group=} values --------")
    print(f"{inventory_element.username=}")
    print(f"{inventory_element.password=}")
    if not isinstance(inventory_element, Defaults):
        print(f"-------- {host_or_group=} items --------")
        print(f"{json.dumps(dict(inventory_element.items()), indent=2)}")
        for connection in ['netmiko', 'napalm', 'scrapli']:
            print(f"-------- {host_or_group=} {connection=} --------")
            print(f"{json.dumps(inventory_element.get_connection_parameters(connection).dict(), indent=2)}")

def parse_arguments() -> argparse.Namespace:
    parser: argparse.ArgumentParser = argparse.ArgumentParser(description='tool to automate network stuff')
    parser.add_argument('template_paths',
                        type=str, nargs='*', action='store',
                        help='template names')
    parser.add_argument('-l', '--limit',
                        type=str, action='store',
                        help='limit inventory with LIMIT')
    parser.add_mutually_exclusive_group().add_argument('-g', '--generate_only',
                        action='store_true',
                        help='only generate')
    parser.add_argument('-I', '--interactive',
                        action='store_true',
                        help='get prompted')
    parser.add_argument('-B', '--backup',
                        action='store_true',
                        help='backup yes')
    parser.add_mutually_exclusive_group().add_argument('-C', '--check',
                        action='store_true',
                        help='dry run')
    parser.add_argument('-A', '--ansible',
                        action='store_true',
                        help='deploy ansible playbooks')
    parser.add_argument('--privexec',
                        action='store_true',
                        help='force starting on privEXEC mode')
    arguments: argparse.Namespace = parser.parse_args()
    return arguments

def run_commands(
    task: Task,
    template_dir: str,
    inventory_dir: str,
    chosen_template: str,
    enable_GENERATEonly: bool,
    enable_BACKUP: bool,
    backup_dir_name: str,
    enable_CHECKonly: bool,
    enable_privEXEConly: bool,
) -> None:
    render_templates(
        task,
        template_dir,
        inventory_dir,
        chosen_template,
        enable_GENERATEonly,
        enable_BACKUP,
        backup_dir_name,
    )
    avoid_config_mode: list[str] = []
    if chosen_template.startswith(("fullCONFIG", "partialCONFIG", "CFG")) and not task.host["privexec_only"] and not enable_privEXEConly:
        if chosen_template.startswith("fullConfig"):
            enable_REPLACE: bool = True
        else:
            enable_REPLACE: bool = False
        output: MultiResult = task.run(task=netmiko_send_config, config_commands=task.host["config"], enable=True, dry_run=enable_CHECKonly)
    else:
        output: MultiResult = task.run(task=netmiko_send_command, command_string=task.host["config"], enable=True)
    if enable_BACKUP:
        backup_dir: str = f"{os.path.join(inventory_dir, '..', backup_dir_name, chosen_template.replace('.j2', ''))}"
        os.makedirs(backup_dir, exist_ok=True)
        backup_filename: str = f"{backup_dir}/{task.host.name}__{chosen_template.replace('.j2', '')}.txt"
        task.run(task=write_file, content=output.result, filename=backup_filename)

def render_templates(
    task: Task,
    template_dir: str,
    inventory_dir: str,
    chosen_template: str,
    enable_GENERATEonly: bool,
    enable_BACKUP: bool,
    backup_dir_name: str,
) -> None:
    filename: str = chosen_template
    r: MultiResult = task.run(
        task=template_file,
        name=chosen_template,
        template=filename,
        path=template_dir,
        **task.host,
    )
    task.host["config"] = r.result
    if enable_GENERATEonly and enable_BACKUP:
        backup_dir: str = f"{os.path.join(inventory_dir, '..', backup_dir_name, chosen_template.replace('.j2', ''))}"
        os.makedirs(backup_dir, exist_ok=True)
        backup_filename: str = f"{backup_dir}/{task.host.name}__{chosen_template.replace('.j2', '')}.txt"
        task.run(task=write_file, content=task.host["config"], filename=backup_filename)

def create_limited_nr(nr: Nornir, limit_list: list[str]) -> Nornir:
    include_lists: list[list[str]] = []
    exclude_list: list[str] = []
    temp_list: list[str] = []
    f = None
    limited_nr: Nornir = nr
    for item in limit_list:
        item = item.replace('*', '')
        if item.startswith('!'):
            item = item.replace('!', '')
            exclude_list.append(item)
        elif item.startswith('&'):
            item = item.replace('&', '')
            if temp_list:
                temp_list.append(item)
        else:
            if temp_list:
                include_lists.append(temp_list)
            temp_list = [item]
    if temp_list:
        include_lists.append(temp_list)
    for include_list in include_lists:
        if len(include_list) == 1:
            if f is None:
                f = (F(groups__contains=include_list[0]) | F(name__contains=include_list[0]))
            else:
                f = f | (F(groups__contains=include_list[0]) | F(name__contains=include_list[0]))
        else:
            if f is None:
                f = ((F(groups__contains=include_list[0]) | F(name__contains=include_list[0])) & F(groups__all=include_list[1:]))
            else:
                f = f | ((F(groups__contains=include_list[0]) | F(name__contains=include_list[0])) & F(groups__all=include_list[1:]))
    limited_nr = nr.filter(f)
    for exclude_item in exclude_list:
        ex = ~F(groups__contains=exclude_item) & ~F(name__contains=exclude_item)
        limited_nr = limited_nr.filter(ex)
    return limited_nr

def add_ansible_variables(nr: Nornir, inventory_dir, runbook_dir) -> Nornir:
    nr.inventory.defaults.data['inventory_dir'] = inventory_dir
    nr.inventory.defaults.data['runbook_dir'] = runbook_dir
    #nr.inventory.defaults.data['playbook_dir'] = os.path.abspath(os.environ.get('ANSIBLE_PLAYBOOK_DIR') or '')
    nr.inventory.defaults.data['playbook_dir'] = f"{os.path.join(runbook_dir, 'ansible')}{os.sep}"
    for host_name, host in nr.inventory.hosts.items():
        host.data['inventory_hostname'] = host_name
        host.data['ansible_host'] = host.hostname
        host.data['ansible_user'] = host.username
        host.data['group_names'] = [group.name for group in host.groups]
    if 'ansible_become_password' in nr.inventory.defaults.data:
        for host in nr.inventory.hosts:
            add_become_password(nr, host=host, become_password=nr.inventory.defaults.data['ansible_become_password'])
    return nr

def add_encrypted_data(nr: Nornir, encrypted_file) -> Nornir:
    gpg: gnupg.GPG = gnupg.GPG()
    ssh_password: str | None = None
    become_password: str | None = None
    if os.path.isfile(encrypted_file):
        with open(encrypted_file, 'rb') as p:
            encrypted_data: bytes = p.read()
        decrypted_data = gpg.decrypt(encrypted_data)
        parsed_password_data = yaml.safe_load(str(decrypted_data))
        decrypted_data = None
        if ('vars' in parsed_password_data['all'] and 
                'ansible_password' in parsed_password_data['all']['vars']
        ):
            nr.inventory.defaults.password = (
                parsed_password_data.get('all', {})
                .get('vars', {})
                .get('ansible_password')
            )
        if ('vars' in parsed_password_data['all'] 
                and 'ansible_become_password' in parsed_password_data['all']['vars']
        ):
            become_password = (
                parsed_password_data.get('all', {})
                .get('vars', {})
                .get('ansible_become_password')
            )
            for host in nr.inventory.hosts:
                add_become_password(nr, host=host, become_password=become_password)
        if 'children' in parsed_password_data['all']:
            for group in parsed_password_data['all']['children']:
                if ('vars' in parsed_password_data['all']['children'][group] and
                        'ansible_password' in parsed_password_data['all']['children'][group]['vars']
                ):
                    nr.inventory.groups[group].password = (
                        parsed_password_data.get('all', {})
                        .get('children', {})
                        .get(group, {})
                        .get('vars', {})
                        .get('ansible_password')
                    )
                if ('vars' in parsed_password_data['all']['children'][group] and
                        'ansible_become_password' in parsed_password_data['all']['children'][group]['vars']
                ):
                    become_password = (
                        parsed_password_data.get('all', {})
                        .get('children', {})
                        .get(group, {})
                        .get('vars', {})
                        .get('ansible_become_password')
                    )
                    for host in nr.inventory.hosts.values():
                        if group in host.groups:
                            add_become_password(nr, host=str(host), become_password=become_password)
                if ('children' in parsed_password_data['all'] and 
                    group in parsed_password_data['all']['children'] and 
                    'hosts' in parsed_password_data['all']['children'][group]
                ):
                    for host in parsed_password_data['all']['children'][group]['hosts']:
                        if 'ansible_password' in parsed_password_data['all']['children'][group]['hosts'][host]:
                            nr.inventory.hosts[host].password = (
                                parsed_password_data.get('all', {})
                                .get('children', {})
                                .get(group, {})
                                .get('hosts', {})
                                .get(host, {})
                                .get('ansible_password')
                            )
                        if 'ansible_become_password' in parsed_password_data['all']['children'][group]['hosts'][host]:
                            become_password = (
                                parsed_password_data.get('all', {})
                                .get('children', {})
                                .get(group, {})
                                .get('hosts', {})
                                .get(host, {})
                                .get('ansible_become_password')
                            )
                            add_become_password(nr, host=host, become_password=become_password)

    if nr.inventory.defaults.password == None:
        ssh_password = getpass('SET default ssh_password: ')
        nr.inventory.defaults.password = ssh_password
    default_become_is_set: bool = False
    for host in nr.inventory.hosts.values():
        if host.connection_options != {}:
            if 'netmiko' in host.connection_options:
                nr.inventory.hosts[str(host)].connection_options['netmiko'].extras = nr.inventory.hosts[str(host)].connection_options['netmiko'].extras or {}
                if ('secret' in nr.inventory.hosts[str(host)].connection_options['netmiko'].extras.keys()
                    and nr.inventory.hosts[str(host)].connection_options['netmiko'].extras['secret'] != None
                ):
                    continue
                else:
                    ask_become_password_if_not_set(nr, host=str(host), become_password=become_password, default_become_is_set=default_become_is_set)
            else:
                ask_become_password_if_not_set(nr, host=str(host), become_password=become_password, default_become_is_set=default_become_is_set)
        else:
            ask_become_password_if_not_set(nr, host=str(host), become_password=become_password, default_become_is_set=default_become_is_set)
    # become password gets asked multiple times!!!!  not intended
    ssh_password = None
    become_password = None
    parsed_password_data = None
    return nr

def ask_become_password_if_not_set(
    nr: Nornir,
    *,
    host: str,
    become_password: str | None = None,
    default_become_is_set: bool,
)-> bool:
    if default_become_is_set:
        add_become_password(nr, host=host, become_password=become_password)
    else:
        become_password = getpass('SET default become_password: ')
        default_become_is_set = True
        add_become_password(nr, host=host, become_password=become_password)
    return default_become_is_set

def add_become_password(
    nr: Nornir,
    *,
    host: str,
    become_password: str | None = None,
)-> None:
    inventory_host = nr.inventory.hosts[host]

    if inventory_host.connection_options.setdefault(
        'netmiko', ConnectionOptions(
            extras={'secret': become_password}
        )
    ).extras != None:
        inventory_host.connection_options['netmiko'].extras['secret'] = become_password
    if inventory_host.connection_options.setdefault(
        'napalm', ConnectionOptions(
            extras={'optional_args': {'secret': become_password}}
        )
    ).extras != None:
        inventory_host.connection_options['napalm'].extras.setdefault('optional_args', {})['secret'] = become_password
    if inventory_host.connection_options.setdefault(
        'scrapli', ConnectionOptions(
            extras={'auth_secondary': become_password}
        )
    ).extras != None:
        inventory_host.connection_options['scrapli'].extras['auth_secondary'] = become_password

def sync_connection_options(nr: Nornir, connection_list: list[str]) -> Nornir:
    for host in nr.inventory.hosts.values():
        for group in host.groups:
            for connection in connection_list:
                if connection in nr.inventory.groups[str(group)].connection_options:
                    if nr.inventory.groups[str(group)].connection_options[connection].extras != None:
                        if connection in host.connection_options:
                            host.connection_options[connection].extras = host.connection_options[connection].extras or {}
                            host.connection_options[connection].extras = nr.inventory.groups[str(group)].connection_options[connection].extras | host.connection_options[connection].extras
    return nr

def create_autocompleter(templates: list[str]) -> NestedCompleter:
    completer_dict: dict[t.Any, t.Any] = {}
    for template in templates:
        keys: list[str] = template.replace(".j2", "").split("_")
        nested_dict: dict = completer_dict
        for k in keys[:-1]:
            if k not in nested_dict:
                nested_dict[k] = {}
            nested_dict = nested_dict[k]
        if nested_dict is not None:
            nested_dict[keys[-1]] = None
    completer = NestedCompleter.from_nested_dict(completer_dict)
    return completer

def bool_question(question: str, default: bool = False) -> bool:
    """
    [TODO:description]

    :param question: [TODO:description]
    :param default: [TODO:description]
    :return: [TODO:description]
    """
    while True:
        if default == True:
            input_options_bool: list[str] = ['false', '0', 'no', 'n']
        else:
            input_options_bool = ['true', '1', 'yes', 'y']
        user_input: str = input(f"{question}\n  Type {input_options_bool} (or nothing for default) and press Enter [default={default}]: ")
        if user_input == '':
            return default
        elif user_input.lower() not in ['true', 'false', '1', '0', 'yes', 'no', 'y', 'n']:
            print(f"Invalid input. Please type {input_options_bool} (or nothing for default) and press Enter [default={default}].")
        elif user_input.lower() in input_options_bool:
            return not default
        else:
            return default

def ansible_mode(
    chosen_templates: list[str],
    template_dir,
    runbook_dir,
    encrypted_file,
    enable_GENERATEonly: bool,
    enable_BACKUP: bool,
    enable_CHECKonly: bool,
    enable_privEXEConly: bool,
    enable_LIMIT: bool,
    user_input_limitstring: str,
) -> None:
    vault_password: str | None = None
    #completer: NestedCompleter = create_autocompleter(templates)
    gpg: gnupg.GPG = gnupg.GPG()
    #ansible_playbooks: list[str] = os.listdir(f"{os.path.join(runbook_dir, 'ansible')}{os.sep}")
    ansible_config: str = f"{os.path.join(runbook_dir, 'ansible', 'ansible.cfg')}"
    #playbook_completer = WordCompleter(ansible_playbooks)
    #chosen_playbook_command: str = prompt("$ ansible-playbook playbook_dir/", completer=playbook_completer).rstrip()
    #chosen_templates: list[str] = [prompt("\nchooseTemplate# ", completer=completer).rstrip().replace(' ', '_')] # take list on command line too?
    #if chosen_temp
    for template in chosen_templates:
        #ansible_command: str = f"ANSIBLE_CONFIG={ansible_config} ansible-playbook --extra-vars \"template_dir={template_dir}/ chosen_template={chosen_templates[0]}\" {os.path.join(runbook_dir, 'ansible')}{os.sep}{chosen_playbook_command} --skip-tags \"templating\""
        chosen_playbook: str = ''
        if enable_GENERATEonly:
            chosen_playbook = 'generate.yml'
        elif enable_privEXEConly:
            chosen_playbook = 'cli.yml'
        elif 'fullCONFIG' in template:
            chosen_playbook = 'replace-config.yml'
        elif 'CFG' in template or 'partialCONFIG' in template:
            chosen_playbook = 'config.yml'
        else:
            chosen_playbook = 'cli.yml'
        ansible_command: str = (
            f"ANSIBLE_CONFIG={ansible_config} "
            f"ansible-playbook "
            f"--extra-vars \"template_dir={template_dir}/ chosen_template={template.removesuffix('.j2')} enable_BACKUP={enable_BACKUP}\" "
            f"{os.path.join(runbook_dir, 'ansible')}{os.sep}{chosen_playbook} "
            f"--skip-tags \"templating\""
        )
        if enable_LIMIT:
            ansible_command = f"{ansible_command} --limit '{user_input_limitstring}'"
        if enable_CHECKonly:
            ansible_command = f"{ansible_command} --check --diff"
        if os.path.isfile(encrypted_file):
            with open(encrypted_file, 'rb') as p:
                encrypted_data: bytes = p.read()
            decrypted_data = gpg.decrypt(encrypted_data)
            parsed_password_data = yaml.safe_load(str(decrypted_data))
            decrypted_data = None
            if ('vars' in parsed_password_data['all'] and
                    'ansible_vault_password' in parsed_password_data['all']['vars']
            ):
                vault_password = (
                    parsed_password_data.get('all', {})
                    .get('vars', {})
                    .get('ansible_vault_password')
                )
        with tempfile.NamedTemporaryFile(mode='w', delete=True) as temp_file:
            temp_file.write(str(vault_password))
            temp_file.flush()
            ansible_command = f"{ansible_command} --vault-password-file {temp_file.name}"
            os.system(ansible_command)

def main() -> None:
    user_input_limitstring: str = ''
    arguments: argparse.Namespace = parse_arguments()
    if not arguments.generate_only and arguments.interactive:
        enable_GENERATEonly: bool = bool_question("Do you want to only GENERATE output from templates without actually connecting to devices?")
    else:
        enable_GENERATEonly = arguments.generate_only
    if not arguments.privexec and arguments.interactive:
        enable_privEXEConly: bool = bool_question("Do you want to force starting from privEXEC mode?")
    else:
        enable_privEXEConly = arguments.privexec
    if not arguments.check and arguments.interactive:
        enable_CHECKonly: bool = bool_question("Do you want to only CHECK the outcome without actually executing on devices?")
    else:
        enable_CHECKonly = arguments.check
    if not arguments.limit and arguments.interactive:
        enable_LIMIT: bool = bool_question("Do you want to LIMIT the inventory? Otherwise this script is run on all devices in inventory.")
        if enable_LIMIT:
            print()
            user_input_limitstring = input("Type the groups and hosts you want included, separated with ':'. Groups that start with '&' farther limit the last element without starting parameters. Start hosts and groups that should be explicitly excluded with '!'.\nexample: site1:&spines:!spine4_site1:host3_site2\n\t: ")
    elif arguments.limit:
        enable_LIMIT = True
        user_input_limitstring = arguments.limit
    else:
        enable_LIMIT = False
    if not arguments.backup and arguments.interactive:
        enable_BACKUP: bool = bool_question("Do you want to BACKUP the output?")
    else:
        enable_BACKUP = arguments.backup

    backup_dir_name: str = ''
    inventory_files: list[str] = []
    encrypted_files: list[str] = []
    runbook_dir: str = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
    networkcaretaker_site: str = os.path.abspath(os.environ.get('NETWORKCARETAKER_SITE') or '')
    #inventory_dir = os.path.abspath(os.environ.get('ANSIBLE_INVENTORY') or '')
    inventory_dir: str = f"{os.path.join(networkcaretaker_site, 'inventories')}{os.sep}"
    for file in os.listdir(inventory_dir):
        if file.endswith('.yml') or file.endswith('.yaml'):
            inventory_files.append(os.path.join(inventory_dir, file))
            secret: str = f"{file.replace('inventory', 'secrets')}.gpg"
            encrypted_files.append(os.path.join(inventory_dir, secret))
    general_template_dir: str = f"{os.path.join(runbook_dir, 'templates')}{os.sep}"
    custom_template_dir: str = f"{os.path.join(inventory_dir, '..', 'templates')}{os.sep}"
    temp_dir = tempfile.mkdtemp()
    template_dir = os.path.join(temp_dir, 'merged_templates')
    os.makedirs(template_dir)
    for src_dir in [d for d in [general_template_dir, custom_template_dir] if os.path.isdir(d)]:
        for item in os.listdir(src_dir):
            src_path = os.path.join(src_dir, item)
            dest_path = os.path.join(template_dir, item)
            if os.path.isdir(src_path):
                shutil.copytree(src_path, dest_path)
            else:
                shutil.copy2(src_path, dest_path)
    templates: list[str] = [f for f in os.listdir(template_dir) if f.endswith(".j2")]

    if not arguments.template_paths:
        completer: NestedCompleter = create_autocompleter(templates)
        while True:
            chosen_templates: list[str] = [prompt("\nchooseTemplate# ", completer=completer).rstrip().replace(' ', '_') + '.j2']
            #chosen_input: str = prompt("\nchooseTemplate# ", completer=completer).rstrip()
            #chosen_templates: list[str] = [chosen_input.replace(' ', '_') + '.j2']
            if chosen_templates[0] in templates: # can i check all?
                break
            else:
                print(f"Invalid input. Template does not exist.")
        print('You chose: %s\n' % chosen_templates)
    else:
        chosen_templates = []
        chosen_template_paths: list[str] = arguments.template_paths
        for path in chosen_template_paths:
            chosen_templates.append(path.split(os.sep)[-1])

    if arguments.ansible:
        ansible_mode(
            chosen_templates,
            template_dir,
            runbook_dir,
            encrypted_files[0],
            enable_GENERATEonly,
            enable_BACKUP,
            enable_CHECKonly,
            enable_privEXEConly,
            enable_LIMIT,
            user_input_limitstring,
        )
        exit()

    os.chdir(runbook_dir)

    InventoryPluginRegister.register("AnsibleInventory", AnsibleInventory)
    if enable_GENERATEonly:
        nr: Nornir = InitNornir(
            runner={
                "plugin": "threaded",
                "options": {
                    "num_workers": 100,
                }
            },
            logging={
                "log_file": os.path.join(inventory_dir, '..', 'nornir.log')
            },
            inventory={
                "plugin": "AnsibleInventory", "options": {
                    "hostsfile": os.path.join(inventory_dir, inventory_files[0])
                },
            },
            #ssh={
            #    "config_file": os.path.join(inventory_dir, '..', '.ssh', 'config')
            #},
        )
    else:
        nr: Nornir = InitNornir(
            runner={
                "plugin": "RetryRunner",
                "options": {
                    "num_workers": 100,
                    "num_connectors": 10,
                    "connect_retry": 3,
                    "connect_backoff": 1000,
                    "connect_splay": 100,
                    "task_retry": 3,
                    "task_backoff": 1000,
                    "task_splay": 100
                }
            },
            logging={
                "log_file": os.path.join(inventory_dir, '..', 'nornir.log')
            },
            inventory={
                "plugin": "AnsibleInventory", "options": {
                    "hostsfile": os.path.join(inventory_dir, inventory_files[0])
                },
            },
            #ssh={
            #    "config_file": os.path.join(inventory_dir, '..', '.ssh', 'config')
            #},
        )
    nr = add_ansible_variables(nr, inventory_dir, runbook_dir)
    nr = add_encrypted_data(nr, os.path.join(inventory_dir, encrypted_files[0]))
    nr = sync_connection_options(nr, ['netmiko', 'napalm', 'scrapli'])

    if enable_LIMIT:
        limit_list: list[str] = [x for x in user_input_limitstring.split(':')]
        limited_nr = create_limited_nr(nr, limit_list)
    else:
        limited_nr = nr

    if enable_BACKUP:
        if enable_GENERATEonly:
            backup_dir_name = f"BACKUPs{os.sep}GENERATEd"
        else:
            backup_dir_name = 'BACKUPs'

    os.chdir(os.path.join(inventory_dir, '..')) #still necessary after logfile dir has been defined?

    for chosen_template in chosen_templates:
        if enable_GENERATEonly:
            results: AggregatedResult = limited_nr.run(
                task=render_templates,
                template_dir=template_dir,
                inventory_dir=inventory_dir,
                chosen_template=chosen_template,
                enable_GENERATEonly=enable_GENERATEonly,
                enable_BACKUP=enable_BACKUP,
                backup_dir_name=backup_dir_name,
            )
        else:
            results: AggregatedResult = limited_nr.run(
                task=run_commands,
                template_dir=template_dir,
                inventory_dir=inventory_dir,
                chosen_template=chosen_template,
                enable_GENERATEonly=enable_GENERATEonly,
                enable_BACKUP=enable_BACKUP,
                backup_dir_name=backup_dir_name,
                enable_CHECKonly=enable_CHECKonly,
                enable_privEXEConly=enable_privEXEConly,
            )
        print_result(results)
    shutil.rmtree(template_dir)

if __name__ == '__main__':
    main()
