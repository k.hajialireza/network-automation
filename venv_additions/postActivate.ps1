# put this in your <venv_path>\bin\Activate.ps1 (install.sh will do this automatically)
#$postactivatePath = Join-Path $PSScriptRoot "postActivate.ps1"
#if (Test-Path $postactivatePath) {
#    . $postactivatePath
#}

if (-not ([Security.Principal.WindowsPrincipal]([Security.Principal.WindowsIdentity]::GetCurrent())).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator))
{
	$env:ANSIBLE_PLAYBOOK_DIR = "$HOME\network-automation\ansible\"
	$env:ANSIBLE_INVENTORY = "$HOME\network-automation\example-multivendorLab_showcase\inventories\"
	$env:ANSIBLE_LOG_PATH = "$($env:ANSIBLE_INVENTORY)..\ansible.log"  # first inventory in ansible_inventory
	$env:ANSIBLE_ASK_VAULT_PASS = True
	$env:ANSIBLE_HOST_KEY_CHECKING = False # set to true when you have all host keys
	#$env:ANSIBLE_SSH_ARGS = "-F $($env:ANSIBLE_INVENTORY)..\.ssh\config" #proxycommand not necessary when this in place?
	#$env:ANSIBLE_PRIVATE_KEY_FILE = 
	#$env:ANSIBLE_NETCONF_SSH_CONFIG = 
	#$env:ANSIBLE_JINJA2_EXTENSIONS = 
}
