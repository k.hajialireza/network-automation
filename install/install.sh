#/usr/bin/env bash

if grep -q '^ID=fedora' /etc/os-release; then
	sudo dnf upgrade -y
	sudo dnf install -y git
else
	sudo apt update
	sudo apt upgrade -y
	sudo apt install -y git
fi

if [[ $PWD != */network-automation/install ]]; then
	cd ~
	git clone https://gitlab.com/k.hajialireza/network-automation
	cd ~/network-automation/install
fi

if grep -q '^ID=fedora' /etc/os-release; then
	cat ./fedora_dependencies.txt | sudo xargs dnf install -y
else
	cat ./debian_dependencies.txt | sudo xargs apt install -y
fi

python3 -m pip install --upgrade certifi
python3 -m pip install --upgrade pip
python3 -m venv ~/.venv_network
. ~/.venv_network/bin/activate
python3 -m pip install --upgrade pip
cat ./python* | xargs python3 -m pip install --upgrade

xargs -a ansible_collections.txt ansible-galaxy collection install

mkdir -p ~/bin
ln -s ~/network-automation/network-caretaker.py ~/bin/network-caretaker.py

cp ../venv_additions/postactivate ~/.venv_network/bin/
echo '' >> ~/.venv_network/bin/activate
head -n 4 ~/.venv_network/bin/postactivate | tail -n 3 | sed 's/^#//' >> ~/.venv_network/bin/activate

